//alternative for query selectors

//document.getElementById('txt-first-name')
//document.getElementsByClassName('txt-inputs')
//document.getElementsByTagName('input')


//To retrieve an element from the web page:

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
})


//Multiple listeners can also be assigned to the same event

txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target); //access the object model
	console.log(e.target.value) //access the specific value
})



// =============================================

//Session 43: Reactive DOM with JSON

let posts = [];
let count = 1;

//Add post data


document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault(); //to avoid redirection/ reloading of the page

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});


	count++;
	alert('Successfully added');
	console.log(posts);
	showPosts(posts);

})

//Show posts
const showPosts = (posts) => {
	let postEntries ='';

	posts.forEach((post) => {
		postEntries += `

				<div id="post-${post.id}">
					<h3 id="post-title-${post.id}">${post.title}</h3>
					<p id="post-body-${post.id}">${post.body}</p>

					<button>Edit</button>
					<button>Delete</button>
				</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}








